const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schemas/schema');
const mongoose = require('mongoose');
const options = require('../config');
const cors = require('cors');

const app = express();

//allow cors
app.use(cors());

mongoose.connect(options.dbConnectionString);
mongoose.connection.once('open', () => {
    console.log('🚀 Connected to mLab DB')
})

app.use('/thirtyseven-api', graphqlHTTP({
    schema,
    graphiql: true,
}));

app.listen(4000, () => {
    console.log(`🚀 Server ready at http://localhost:4000`);
});
