const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const todoSchema = new Schema({
    title: String,
    priority: Number,
    asigneeId: String,
    creatorId: String,
});

module.exports = mongoose.model('ToDo', todoSchema);
