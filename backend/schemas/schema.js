const graphql = require('graphql');
const Todo = require('../models/todo');
const Person = require('../models/person');

const { 
    GraphQLID,
    GraphQLObjectType, 
    GraphQLString, 
    GraphQLInt, 
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull
} = graphql;

const ToDoType = new GraphQLObjectType({
    name: "ToDo",
    fields: () => ({ 
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        priority: { type: GraphQLInt },
        asignee: {
            type: PersonType,
            resolve(parent, args){
                return Person.findById(parent.asigneeId);
            }
        },
        creator: {
            type: PersonType,
            resolve(parent, args){
                return Person.findById(parent.creatorId);
            }
        }
    })
});

const PersonType = new GraphQLObjectType({
    name: "Person",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt }, 
        todos: {
            type: new GraphQLList(ToDoType),
            resolve(parent, args){
                return Todo.find({ asigneeId: parent.id })
            }
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        todos: {
            type: new GraphQLList(ToDoType),
            resolve(){
                return Todo.find({});
            }
        },
        todo: {
            type: ToDoType,
            args: { id: { type: GraphQLID }},
            resolve(parent, args){
                return Todo.findById(args.id);
            }
        },
        person: {
            type: PersonType,
            args: { id: { type: GraphQLID }},
            resolve(parent, args){
                return Person.findById(args.id);
            }
        },
        people: {
            type: new GraphQLList(PersonType),
            resolve(){
                return Person.find({});
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addPerson: {
            type: PersonType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) }
            },
            resolve(parent, args){
                let person = new Person({
                    name: args.name,
                    age: args.age
                });
                return person.save();
            }
        },
        addToDo: {
            type: ToDoType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                priority: { type: GraphQLInt },
                asigneeId: { type: GraphQLID },
                creatorId: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(parent, args){
                let todo = new Todo({
                    title: args.title,
                    priority: args.priority,
                    asigneeId: args.asigneeId,
                    creatorId: args.creatorId
                });
                return todo.save();
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})
