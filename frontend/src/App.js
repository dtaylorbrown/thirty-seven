import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// Components
import TodoList from './components/ToDoList';
import AddToDo from './components/AddToDo';

// Apollo Client
const client = new ApolloClient({
  uri: 'http://localhost:4000/thirtyseven-api',
})

function App() {
  return (
    <ApolloProvider client={client} >
      <div className="App">
        <h1>To Do List</h1>
        <TodoList />
        <AddToDo />
      </div>
    </ApolloProvider>
  );
}

export default App;
