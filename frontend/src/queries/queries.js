import { gql } from 'apollo-boost';

const getPeopleQuery = gql`{
    people {
        id
        name
    }
}`;

const getToDosQuery = gql`{
    todos {
        id
        title
        priority
        asignee {
          id
          name
        }
        creator {
          id
          name
        }
    }
}`;

// @TODO currently hard coded creator id - refactor at some point...
const addToDoMutation = gql`
    mutation($title: String!, $priority: Int!, $assigneeID: ID!) {
        addToDo(title:$title, priority:$priority, asigneeId:$assigneeID, creatorId: "5cf434e43e4937e7f73bb0da" ){
            id
            title
            priority
        }
    }
`;

//you are an idiot, please go back and spell assignee correctly....
const getToDoQuery = gql`
    query($id: ID){
        todo(id: $id){
            id
            title
            priority
            asignee {
                id
                name
                todos {
                    title
                    priority
                }
            }
            creator {
                id
                name
            }
        }
    }
`;

export {
    getPeopleQuery,
    getToDosQuery,
    addToDoMutation,
    getToDoQuery
};