import React from 'react';
import { graphql } from 'react-apollo';

import { getToDosQuery } from '../queries/queries';

//components
import ToDoDetails from './ToDoDetails';

class TodoList extends React.Component {

    state = {
        todoId: null,
    }

    loadToDo = (e) => {
        const id = e.target.id;
        this.setState({todoId: id});
    }

    displayTodos(){
        const { data } = this.props;
        console.log(data);
        if(data.error) return <p>Error loading To Do List...</p>
        if(data.loading){
            return(
                <li>Loading To Do List...</li>
            )
        } else {
            return data.todos.map(todo => {
                return(
                    <li
                        key={todo.id}
                        id={todo.id}
                        onClick={this.loadToDo}
                    >
                        {todo.title}
                    </li>
                )
            })
        }
    }

    render(){
        return(
            <>
                <ul>
                    {this.displayTodos()}
                </ul>
                <ToDoDetails id={this.state.todoId}/>
            </>
        )
    }
}

export default graphql(getToDosQuery)(TodoList);