import React from 'react';
import { graphql, compose } from 'react-apollo';

import { getPeopleQuery, addToDoMutation } from '../queries/queries';
import { getToDosQuery } from '../queries/queries';

class AddToDo extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            title: '',
            priority: '',
            assigneeID: '',
        }
    }

    getPeople(){
        const { people, loading } = this.props.getPeopleQuery;
        if(loading){
            return(<option disabled>Loading People...</option>)
        } else {
            return people.map(person => {
                return(
                    <option key={person.id} value={person.id}>{person.name}</option>
                )
            })
        }
    }

    submitForm(e){
        e.preventDefault();
        this.props.addToDoMutation({
            variables: {
                title: this.state.title,
                priority: parseInt(this.state.priority),
                assigneeID: this.state.assigneeID
            },
            refetchQueries: [{query: getToDosQuery}]
        });
        this.setState({
            title: '',
            priority: '',
            assigneeID: '',
        });
    }

    render(){
        return(
            <form id="add_to_do" onSubmit={this.submitForm.bind(this)}>
                <div className="field">
                    <label>To Do Title</label>
                    <input type="text" value={this.state.title} onChange={(e) => this.setState({ title: e.target.value })}/>
                </div>
                <div className="field">
                    <label>Priority</label>
                    <input type="number" value={this.state.priority} min={1} max={3} onChange={(e) => this.setState({ priority: e.target.value })}/>
                </div>
                <div className="field">
                    <label>Assignee</label>
                    <select value={this.state.assigneeID} onChange={(e) => this.setState({ assigneeID: e.target.value })}>
                        <option value="unassigned">Select Someone...</option>
                        {this.getPeople()}
                    </select>
                </div>    
                <input type="submit" value="+" />              
            </form>
        )
    }
}

export default compose(
    graphql(getPeopleQuery, { name: "getPeopleQuery" }),
    graphql(addToDoMutation, { name: "addToDoMutation" })
)(AddToDo);
