import React from 'react';
import { graphql } from 'react-apollo';

import { getToDoQuery } from '../queries/queries';

class ToDoDetails extends React.Component {

    displayTodoDetails(){
        const { loading, todo } = this.props.data;
        if(!todo || loading){
            return null;
        } else {
            console.log(todo);
            return(
                <p>{todo.title}</p>
            )
        }
    }

    render(){
        return(
            <>{this.displayTodoDetails()}</>
        )
    }
}

export default graphql(getToDoQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.id
            }
        }
    }
})(ToDoDetails);

